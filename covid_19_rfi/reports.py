import os
import re
import webbrowser
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import seaborn as sns
from pathlib import Path
from covid_19_rfi.main import validate_all_files


def run():
    # Params
    input_dir = r"C:\\Users\\username\\Documents\\covid_validation\\inputs"
    output_dir = r"C:\\Users\\username\\Documents\\covid_validation\\outputs"

    reports = {'report_file_date_errors': True,
               'report_errors_over_time': True}

    run_reports(input_dir, output_dir, reports)


def run_reports(input_dir, output_dir, reports):
    # Blank line for visual spacing
    print()

    # Nothing to do if there isn't at least one report
    if len([v for k, v in reports.items() if v]) < 1:

        print('Nothing to do! Please select a report.')
        return

    # Input-dir validation
    input_path = Path(input_dir)
    if not input_path.exists():
        print("ERROR: Input directory does not exist or this tool doesn't have read permissions to it. "
              "Please confirm that the input directory provided is correct.")
        return

    # Output-dir validation
    try:
        # Creates the output structure
        output_path = Path(output_dir) / 'reports'
        output_path.mkdir(parents=True, exist_ok=True)

    except PermissionError:
        print("ERROR: Output directory is invalid or this tool doesn't have write permissions to it. "
              "Please confirm that the output directory provided is correct.")
        return

    # Move current directory up so that the relative paths for the main routine remain the same.
    os.chdir(Path.cwd().parent)

    # Timestamp for log and filenames
    str_now = dt.datetime.strftime(dt.datetime.now(), "%Y%m%d_%H%M%S")

    # Validate all files
    validation_results = validate_all_files(input_path, output_path, export_csv=False)

    # Report(s)
    if reports['report_file_date_errors']:
        # List of files with filename or date related errors
        report_name = 'filename_date_errors'
        report_output_path = output_path / '{}_{}.{}'.format(report_name, str_now, 'csv')
        report_filename_date_error(validation_results, report_output_path)

    if reports['report_errors_over_time']:
        # Errors over time
        report_name = 'errors_over_time'
        report_output_path = output_path / '{}_{}.{}'.format(report_name, str_now, 'csv')
        heatmap_output_path = output_path / '{}_{}.{}'.format(report_name, str_now, 'png')
        barplot_output_path = output_path / '{}_bars_{}.{}'.format(report_name, str_now, 'png')
        error_rate_output_path = output_path / '{}_bars_{}.{}'.format('error_rate', str_now, 'png')
        correct_rate_output_path = output_path / '{}_bars_{}.{}'.format('correct_rate', str_now, 'png')
        report_errors_over_time(validation_results, len(input_path.parts), report_output_path, heatmap_output_path,
                                barplot_output_path, error_rate_output_path, correct_rate_output_path)


def report_filename_date_error(validation_results, report_output_path):
    # Lists to store values to be converted to dataframe, so that it is easily exported to CSV file
    ls_filename = []
    ls_content_type = []
    ls_filename_convention_error = []
    ls_date_filename_error = []
    ls_date_column_error = []
    ls_full_path = []

    for file_data in validation_results:
        error_filename_convention = 0
        error_filename_date = 0
        error_date_column = 0

        for error in file_data['errors']:
            error_str = error.lower()
            if 'filename convention' in error_str or 'content matches a different' in error_str:
                error_filename_convention += 1
            elif 'date column' in error_str or 'date at row' in error_str:
                error_date_column += 1
            elif 'date in filename' in error_str:
                error_filename_date += 1

        if error_filename_convention + error_filename_date + error_date_column > 0:
            ls_filename.append(file_data['name'])
            ls_content_type.append(file_data['content_type'])
            ls_filename_convention_error.append(error_filename_convention)
            ls_date_filename_error.append(error_filename_date)
            ls_date_column_error.append(error_date_column)
            ls_full_path.append(file_data['full_path'])

    # Save to CSV
    df_report = pd.DataFrame({'filename': ls_filename,
                              'content_type': ls_content_type,
                              'filename_convention_error': ls_filename_convention_error,
                              'date_filename_error': ls_date_filename_error,
                              'date_column_error': ls_date_column_error,
                              'full_path': ls_full_path})

    df_report.to_csv(report_output_path, encoding='utf-8', index=False)
    print()
    print('Report has been saved at {}'.format(report_output_path))


def report_errors_over_time(validation_results, input_path_offset, report_output_path, heatmap_output_path,
                            barplot_output_path, error_rate_output_path, correct_rate_output_path):
    idx_supplier = input_path_offset
    re_date = re.compile(r'\d{8}|\d{6}|\d{2}\S\d{2}\S\d{2,4}|\d\d\d\d.{0,2}\d\d.{0,2}\d\d.{0,2}|'
                         r'\d\d.{0,2}\d\d.{0,2}\d\d\d\d?')
    start = dt.datetime(2020, 2, 3)
    end = dt.datetime.today().date()
    rng_monday = pd.date_range(start=start, end=end, freq='7D')
    invalid_date = 'week?'

    # Lists to store values to be converted to dataframe, so that it is easily exported to CSV file
    ls_supplier = []
    ls_filename = []
    ls_content_type = []
    ls_week = []
    ls_error_count = []

    print()
    print()
    for file_data in validation_results:
        if file_data['name_type'] == '' and file_data['content_type'] == '':
            continue

        # Week - it tries to infer as much as possible from several date formats
        date_parts = re_date.findall(file_data['name'])
        day, month, year = 0, 0, 0
        if len(date_parts) > 0:
            str_date = re.sub(r'[^0-9]', '', date_parts[0])
            if len(str_date) == 6:
                str_date = str_date + '20'

            if len(str_date) == 8:
                if str_date[:4] == '2020':
                    year = int(str_date[:4])
                    month = int(str_date[4:6])
                    day = int(str_date[6:])
                elif str_date[-4:] == '2020':
                    day = int(str_date[:2])
                    month = int(str_date[2:4])
                    year = int(str_date[4:])

            try:
                filename_date = dt.datetime(year, month, day)
                filename_monday = rng_monday[rng_monday <= filename_date][-1].to_pydatetime().strftime('%d/%m/%Y')
            except ValueError:
                filename_monday = invalid_date
        else:
            filename_monday = invalid_date

        ls_supplier.append(file_data['full_path'].parts[idx_supplier])
        ls_filename.append(file_data['name'])
        ls_content_type.append(file_data['content_type'])
        ls_week.append(filename_monday)

        # Error count
        ls_error_count.append(len(file_data['errors']))

    # Save to CSV
    df_report = pd.DataFrame({'supplier': ls_supplier,
                              'filename': ls_filename,
                              'content_type': ls_content_type,
                              'week': ls_week,
                              'error_count': ls_error_count})

    df_report.to_csv(report_output_path, encoding='utf-8', index=False)
    print()
    print('Report has been saved at {}'.format(report_output_path))

    # Heatmap
    df_rep = df_report.groupby(["supplier", "week"], as_index=False)["error_count"].sum().round()
    df_rep = df_rep.pivot("supplier", "week", "error_count")

    # Sort of string dates and `week?`
    ls = df_rep.columns
    ls_week = []

    if 'week?' in ls:
        ls_week = ['week?']
        ls = [item for item in ls if item != 'week?']

    ls = sorted(ls, key=lambda x: dt.datetime.strptime(x, '%d/%m/%Y'))
    ls = ls + ls_week

    # Plot
    plt.figure(figsize=(10, 40))
    ax = sns.heatmap(df_rep[ls], vmin=0, annot=True, cmap='Reds', fmt='g', linecolor='white', linewidths=.5)
    ax.set_title('Number of Errors per Week', fontsize=18, pad=18)
    ax.set_ylabel('Supplier', fontsize=15, labelpad=18)
    ax.set_xlabel('W/C Monday', fontsize=15, labelpad=18)
    bottom, top = ax.get_ylim()
    ax.set_ylim(bottom + 0.5, top - 0.5)
    ax.set_facecolor('whitesmoke')
    fig = ax.get_figure()

    # Save to PNG file
    fig.savefig(heatmap_output_path, bbox_inches='tight')

    # Open this plot in a web browser (easier to zoom in and scroll in Windows)
    print()
    print('Plot has been saved at {}'.format(heatmap_output_path))

    try:
        # Open the browser with the results
        webbrowser.open(heatmap_output_path, new=2)
    except TypeError:
        # Issue is documented here: https://bugs.python.org/issue30392
        # Print with path already above
        pass

    ######
    # Bar Plot
    ######
    df_rep = df_report.groupby(["week"], as_index=False)["error_count"].sum()

    # Sort of string dates and `week?`
    ls = df_rep['week'].values
    ls_week = []

    if 'week?' in ls:
        ls_week = ['week?']
        ls = [item for item in ls if item != 'week?']

    ls = sorted(ls, key=lambda x: dt.datetime.strptime(x, '%d/%m/%Y'))
    # week? first
    ls = ls_week + ls

    # Plot
    sns.set_style("whitegrid")
    plt.figure(figsize=(12, 8))

    ax = sns.barplot(x='week', y='error_count', data=df_rep, order=ls, color='tomato')

    ax.set_title("Number of Errors per Week", fontsize=18, pad=18)
    ax.set_ylabel("Number of Errors", fontsize=16, labelpad=18)
    ax.set_xlabel("W/C Monday", fontsize=16, labelpad=18)
    bottom, top = ax.get_ylim()
    ax.set_ylim(bottom + 0.5, top - 0.5)

    ax.set_xticklabels(ax.get_xticklabels(), rotation=90, fontsize=14)
    ax.set_yticklabels(["{0:.0f}".format(y) for y in ax.get_yticks()], fontsize=14)

    fig = ax.get_figure()
    # Save to PNG file
    fig.savefig(barplot_output_path, bbox_inches='tight')

    # Open this plot in a web browser (easier to zoom in and scroll in Windows)
    print()
    print('Plot has been saved at {}'.format(barplot_output_path))

    try:
        # Open the browser with the results
        webbrowser.open(barplot_output_path, new=2)
    except TypeError:
        # Issue is documented here: https://bugs.python.org/issue30392
        # Print with path already above
        pass

    ######
    # Error Rate - Bar Plot
    ######
    df_sum = df_report[['week', 'error_count']].groupby(['week']).sum()
    df_count = df_report[['week', 'error_count']].groupby(['week']).count()

    df_rep = df_sum['error_count'] / df_count['error_count']
    df_rep = df_rep.reset_index()

    # Sort of string dates and `week?`
    ls = df_rep['week'].values
    ls_week = []

    if 'week?' in ls:
        ls_week = ['week?']
        ls = [item for item in ls if item != 'week?']

    ls = sorted(ls, key=lambda x: dt.datetime.strptime(x, '%d/%m/%Y'))
    # week? first
    ls = ls_week + ls

    # Plot
    sns.set_style("whitegrid")
    plt.figure(figsize=(12, 8))

    ax = sns.barplot(x='week', y='error_count', data=df_rep, order=ls, color='tomato')

    ax.set_title("Error Rate per Week", fontsize=18, pad=18)
    ax.set_ylabel("Num Errors / Num Submitted Files", fontsize=16, labelpad=18)
    ax.set_xlabel("W/C Monday", fontsize=16, labelpad=18)
    # bottom, top = ax.get_ylim()
    # ax.set_ylim(bottom + 0.5, top - 0.5)

    ax.set_xticklabels(ax.get_xticklabels(), rotation=90, fontsize=14)
    ax.set_yticklabels(ax.get_yticks(), fontsize=14)

    fig = ax.get_figure()
    # Save to PNG file
    fig.savefig(error_rate_output_path, bbox_inches='tight')

    # Open this plot in a web browser (easier to zoom in and scroll in Windows)
    print()
    print('Plot has been saved at {}'.format(error_rate_output_path))

    try:
        # Open the browser with the results
        webbrowser.open(error_rate_output_path, new=2)
    except TypeError:
        # Issue is documented here: https://bugs.python.org/issue30392
        # Print with path already above
        pass

    ######
    # Correct Rate - Bar Plot
    ######
    df_correct = df_report[df_report['error_count'] == 0]
    df_correct = df_correct[['week', 'error_count']].groupby(['week']).count()
    df_count = df_report[['week', 'error_count']].groupby(['week']).count()

    gf = df_correct / df_count
    gf = gf.fillna(0)
    gf = gf.reset_index()

    # Sort of string dates and `week?`
    # same as above

    # Plot
    sns.set_style("whitegrid")
    plt.figure(figsize=(12, 8))

    ax = sns.barplot(x='week', y='error_count', data=gf, order=ls, color='tomato')

    ax.set_title("% Correct Submitted Files per Week", fontsize=18, pad=18)
    ax.set_ylabel("Correct Submitted Files", fontsize=16, labelpad=18)
    ax.set_xlabel("W/C Monday", fontsize=16, labelpad=18)

    ax.set_xticklabels(ax.get_xticklabels(), rotation=90, fontsize=14)
    ax.set_yticklabels(["{0:.0f}%".format(y * 100) for y in ax.get_yticks()], fontsize=14)

    fig = ax.get_figure()

    # Save to PNG file
    fig.savefig(correct_rate_output_path, bbox_inches='tight')

    # Open this plot in a web browser (easier to zoom in and scroll in Windows)
    print()
    print('Plot has been saved at {}'.format(correct_rate_output_path))

    try:
        # Open the browser with the results
        webbrowser.open(correct_rate_output_path, new=2)
    except TypeError:
        # Issue is documented here: https://bugs.python.org/issue30392
        # Print with path already above
        pass


if __name__ == '__main__':
    run()
