import re
import os
import pandas as pd
import datetime as dt
import webbrowser
from pathlib import Path
from jinja2 import Environment, FileSystemLoader


def validate_data(input_dir, output_dir):
    """
    :param input_dir: directory to Covid-19 RFI consumer and financial data files.
    :param output_dir: directory to store validation results and valid Covid-19 RFI CSV files to be submitted.
    :return: directly none, indirectly validation results and valid CSV files stored in `output_dir`.
    """
    ###
    # Path management
    ###
    input_path = Path(input_dir)

    # Creates the output structure
    output_path = Path(output_dir)
    output_path.mkdir(parents=True, exist_ok=True)

    html_output_path = output_path / 'html_outputs'
    html_output_path.mkdir(exist_ok=True)

    valid_csv_path = output_path / 'valid_csvs'
    valid_csv_path.mkdir(exist_ok=True)

    # Sub-directory to save CSVs per run to avoid confusion over the output dir.
    str_now = dt.datetime.strftime(dt.datetime.now(), "%Y%m%d_%H%M%S")
    sub_dir_path = valid_csv_path / 'valid_csvs_{}'.format(str_now)
    sub_dir_path.mkdir(exist_ok=True)

    # Data templates
    templates_path = Path.cwd() / 'covid_19_rfi' / 'input_templates' / 'xlsx'

    # Max length for columns
    supplier_max_len = 120

    ###
    # Files - data structure and initial data as some fields will fill later.
    ###

    # Regex patterns for filenames
    fn_pattern = r"^[^~$](.*?)\s*\_\s*weekly{}\s*\_\s*(0[1-9]|[1-2][0-9]|3[0-1])\s*\_\s*(0[1-9]|1[0-2])\s*\_\s*2020\.(xlsx|xls|csv)$"

    files = [{'type': 'Part A Qualitative',
              'name': '',
              'full_path': '',
              'template_path': templates_path / 'part_a_qualitative.xlsx',
              'regex_pattern': re.compile(fn_pattern.format('consumerqualitative'), re.IGNORECASE),
              'errors': [],
              'warnings': []},
             {'type': 'Part A Quantitative',
              'name': '',
              'full_path': '',
              'template_path': templates_path / 'part_a_quantitative.xlsx',
              'regex_pattern': re.compile(fn_pattern.format('consumerquantitative'), re.IGNORECASE),
              'errors': [],
              'warnings': []},
             {'type': 'Part B Qualitative',
              'name': '',
              'full_path': '',
              'template_path': templates_path / 'part_b_qualitative.xlsx',
              'regex_pattern': re.compile(fn_pattern.format('financialqualitative'), re.IGNORECASE),
              'errors': [],
              'warnings': []},
             {'type': 'Part B Quantitative',
              'name': '',
              'full_path': '',
              'template_path': templates_path / 'part_b_quantitative.xlsx',
              'regex_pattern': re.compile(fn_pattern.format('financialquantitative'), re.IGNORECASE),
              'errors': [],
              'warnings': []}
             ]

    if not input_path.exists():
        print("ERROR: Input directory does not exist!")
        for f in files:
            # Each expected file has a record of this error so that it is shown in the validation results
            f['errors'].append('Input directory does not exist!')

    else:
        input_files = [f.name for f in input_path.iterdir() if f.is_file()]

        # Check filenames
        for f in files:
            results = list(filter(f['regex_pattern'].match, input_files))
            check = len(results)
            if check < 1:
                f['errors'].append('Filename convention not found! - Please check expected filename structure.')
            elif check > 1:
                f['errors'].append("More than one file found! - Please put only one week's submission in the input directory.")
            else:
                # exactly one file
                f['name'] = results[0]
                f['full_path'] = input_path / f['name']

        # From now on only files with a name (i.e., `files['name'] != ''`) are checked.
        files_with_name = [f for f in files if f['name']!='']

        for f in files_with_name:
            print("Checking {} ...".format(f['full_path']))

            # Date in filename must be a Monday between 1st February and today inclusive
            fn_parts = f['name'].split('_')
            fn_year = int(fn_parts[-1][:4])  # 2020
            fn_month = int(fn_parts[-2])
            fn_day = int(fn_parts[-3])

            try:
                fn_date = dt.date(fn_year, fn_month, fn_day)
                start_date = dt.date(2020, 2, 1)  # 1st February 2020
                end_date = dt.date.today()

                if fn_date.weekday() != 0 or not start_date <= fn_date <= end_date:  # 0: Monday
                    f['errors'].append('Date in filename must be a Monday between 1st February 2020 and today.')

            except ValueError:
                f['errors'].append('Date in filename is invalid.')

            ###
            # Schema checks
            ###

            # Read files, but it doesn't cast anything, everything is kept as in the Excel files
            if f['full_path'].suffix.lower() == '.csv':
                try:
                    df_input = pd.read_csv(f['full_path'], dtype=str, na_filter=False, parse_dates=False, encoding='iso-8859-1')
                    if len(df_input.columns) == 1:
                        # It may be the case that the file uses tabs instead of commas, so it gives it a second try
                        df_input = pd.read_csv(f['full_path'], sep='\t', dtype=str, na_filter=False, parse_dates=False,
                                               encoding='iso-8859-1')
                except pd.errors.ParserError:
                    # This might be a CSV with tabs or there may be characters that need escaping.
                    # Another try with tab separators, but if there's an error, don't catch it to get some feedback
                    df_input = pd.read_csv(f['full_path'], sep='\t', dtype=str, na_filter=False, parse_dates=False,
                                           encoding='iso-8859-1')

            elif f['full_path'].suffix.lower() in ['.xlsx', '.xls']:
                df_input = pd.read_excel(f['full_path'], dtype=str, na_filter=False, parse_dates=False, encoding='iso-8859-1')

            df_template = pd.read_excel(f['template_path'], dtype=str, na_filter=False, parse_dates=False, encoding='iso-8859-1')

            # Remove whitespaces to the left and to the right from strings
            df_input = df_input.applymap(lambda x: x.strip() if isinstance(x, str) else x)
            df_template = df_template.applymap(lambda x: x.strip() if isinstance(x, str) else x)

            # It changes all column names to lowercase to avoid case-sensitive issues
            df_input.rename(columns={c: c.lower() for c in df_input.columns}, inplace=True)
            df_template.rename(columns={c: c.lower() for c in df_template.columns}, inplace=True)

            # Number of columns must be the same as the number of columns in the template
            num_input_cols = len(df_input.columns)
            num_template_cols = len(df_template.columns)
            if num_input_cols < num_template_cols:
                f['errors'].append('Input file has fewer columns ({}) than those in the template ({}).'.format(
                    num_input_cols,
                    num_template_cols
                ))
            elif num_input_cols > num_template_cols:
                f['errors'].append('Input file has more columns ({}) than those in the template ({}).'.format(
                    num_input_cols,
                    num_template_cols
                ))
            else:
                # Same number of columns - lenghts must match to compare
                # Columns - check that the input file has the same columns as its template
                column_mismatch = df_input.columns != df_template.columns
                if column_mismatch.any() > 0:
                    f['errors'].append('Input column(s) {} must be the same as template column(s) {}.'.format(
                        df_input.columns[column_mismatch].values,
                        df_template.columns[column_mismatch].values)
                    )

            # Number of rows must be the same as the number of rows in the template
            num_input_rows = len(df_input)
            num_template_rows = len(df_template)

            if num_input_rows < num_template_rows:
                f['errors'].append('Input file has fewer rows ({}) than those in the template ({}).'.format(
                    num_input_rows,
                    num_template_rows
                ))
            elif num_input_rows > num_template_rows:
                f['errors'].append('Input file has more rows ({}) than those in the template ({}).'.format(
                    num_input_rows,
                    num_template_rows
                ))

            # There must be a column name 'date'
            if 'date' not in df_input.columns:
                f['errors'].append('Date column is missing.')
            else:
                # Date must be in format DD/MM/YYYY and be the same as the one in the filename
                try:
                    # Cast date column so that it is in the right format when the data frame is saved to CSV
                    df_input['date'] = pd.to_datetime(df_input['date'], dayfirst=True, errors='ignore').dt.strftime('%d/%m/%Y')

                    mismatch = df_input['date'] != fn_date.strftime('%d/%m/%Y')
                    if mismatch.any() > 0:
                        rows = [r + 2 for r in mismatch[mismatch].index.tolist()]  # to match Excel rows
                        f['errors'].append('The date at row(s) {} must match date in filename.'.format(rows))

                except (AttributeError, ValueError):
                    f['errors'].append('Date column has invalid dates or invalid format.')

            # Supplier
            if 'supplier' not in df_input.columns:
                f['errors'].append('Supplier column is missing.')
            else:
                # Supplier should not be nullable
                check = df_input['supplier'].str.len() < 1
                if check.any():
                    rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                    f['errors'].append('The supplier at row(s) {} must not be blank.'.format(rows))

                # Supplier must be text with a maximum length
                check = df_input['supplier'].str.len() > supplier_max_len
                if check.any():
                    rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                    f['errors'].append('The supplier at row(s) {} must not exceed {} characters.'.format(rows, supplier_max_len))

            # Section, Question, and Unit
            check_columns = ['section', 'question', 'unit']
            check_same_values = []
            for col in check_columns:
                if col in df_input.columns:
                    # Don't trim white spaces as in the input columns; these columns should be exactly as the template.
                    check_same_values.append(col)
                else:
                    f['errors'].append('{} column is missing.'.format(col[0].capitalize()+col[1:]))

            if f['type'] in ['Part A Qualitative', 'Part A Quantitative', 'Part B Quantitative']:
                # Number of rows must be the same to be able to compare
                if num_input_rows == num_template_rows:
                    # Sections, Questions, and Units - check that the input file has exactly the same as its template
                    for col_name in check_same_values:
                        mismatch = df_input[col_name] != df_template[col_name]

                        if mismatch.any() > 0:
                            rows = [r + 2 for r in mismatch[mismatch].index.tolist()]  # to match Excel rows
                            f['errors'].append('Input {} at row(s) {} must be the same as those in the template.'.format(
                                col_name, rows))

            ###
            # Specific data-type validation per file
            ###
            if f['type'] == 'Part A Qualitative':
                # Check that relevant columns are not missing
                if 'comment' not in df_input.columns:
                    f['errors'].append('Comment column is missing.')

            elif f['type'] == 'Part A Quantitative':
                # Check that relevant columns are not missing
                missing_col = 0
                if 'value' not in df_input.columns:
                    f['errors'].append('Value column is missing.')
                    missing_col += 1

                if 'unit' not in df_input.columns:
                    # Already check above but still required here without an error message
                    missing_col += 1

                if missing_col < 1:
                    # Values in column `value` can be nullable/blank
                    not_blank = df_input['value'] != ''
                    df_view = df_input.loc[not_blank, ['unit', 'value']]

                    if len(df_view) < 1:
                        # All values are blank
                        pass
                    else:
                        # Numeric: num, percent, GBP
                        numeric_units = ['num', 'percent', 'GBP']
                        df_numeric = df_view.loc[df_view['unit'].isin(numeric_units)]
                        check = pd.to_numeric(df_numeric['value'], errors='coerce').isnull()
                        if check.any():
                            rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                            f['errors'].append("The value at row(s) {} must be numeric. \
                            <b>Please avoid adding commas or non-numeric symbols, such as £, £k, £m, %, etc. \
                            If the question does not apply, \
                            please leave it BLANK (avoid adding N/A or comments to this cell)</b>.".format(rows))

                        # Percent
                        df_percent = df_view.loc[df_view['unit']=='percent']
                        try:
                            percent_value = pd.to_numeric(df_percent['value'])
                            check = (percent_value < 0) | (percent_value > 100)
                            if check.any():
                                rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                                f['errors'].append('The value at row(s) {} must be a percent value between 0 and 100.'.format(rows))

                            # Zero values do not require a warning as they are correct in both scales ([0,1] and [0,100])
                            percent_value = percent_value[percent_value != 0]
                            check = (percent_value >= 0) & (percent_value <= 1)
                            if check.all() and len(check) > 0:
                                rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                                f['warnings'].append("All percent values in this file, row(s) {}, are between 0 and 1. " \
                                                     "This is potentially a mistake. " \
                                                     "Percent values for this part are expected between 0 and 100. " \
                                                     "Please correct if the values you meant should be greater than 1.".format(rows))

                        except ValueError:
                            # In case there are non-numeric value and already accounted for above
                            pass

                        # Text: Y/N or ''
                        if df_input['value'].iloc[13] not in ['Y', 'y', 'N', 'n', '']:
                            f['errors'].append('The value at row 15 must be Y/N or blank.')

                # Comments
                if 'comment' not in df_input.columns:
                    f['errors'].append('Comment column is missing.')
                    missing_col += 1

            elif f['type'] == 'Part B Qualitative':
                # Check that relevant columns are not missing
                missing_col = 0
                if 'response' not in df_input.columns:
                    f['errors'].append('Response column is missing.')
                    missing_col += 1

                if 'unit' not in df_input.columns:
                    # Already check above but still required here without an error message
                    missing_col += 1

                # This not needed for the new template
                # if missing_col < 1:
                #     # Responses may be blank
                #     not_blank = df_input['response'] != ''
                #     df_view = df_input.loc[not_blank, ['unit', 'response']]
                #
                #     if len(df_view) < 1:
                #         # All values are blank
                #         pass
                #     else:
                #         df_gbp = df_view.loc[df_view['unit']=='GBP']
                #         check = pd.to_numeric(df_gbp['response'], errors='coerce').isnull()
                #         if check.any():
                #             rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                #             f['errors'].append("The response at row(s) {} must be numeric. \
                #             <b>Please avoid adding commas or non-numeric symbols, such as £, £k, £m, %, etc. \
                #             If the question does not apply, \
                #             please leave it BLANK (avoid adding N/A or comments to this cell)</b>.".format(rows))

            elif f['type'] == 'Part B Quantitative':
                # Check that relevant columns are not missing
                missing_col = 0
                if 'value' not in df_input.columns:
                    f['errors'].append('Value column is missing.')
                    missing_col += 1

                if 'unit' not in df_input.columns:
                    # Already check above but still required here without an error message
                    missing_col += 1

                if missing_col < 1:
                    # Values in column `value` can be nullable/blank
                    not_blank = df_input['value'] != ''
                    df_view = df_input.loc[not_blank, ['unit', 'value']]

                    if len(df_view) < 1:
                        # All values are blank
                        pass
                    else:
                        # Numeric: percent, GBP
                        numeric_units = ['percent', 'GBP']
                        df_numeric = df_view.loc[df_view['unit'].isin(numeric_units)]
                        check = pd.to_numeric(df_numeric['value'], errors='coerce').isnull()
                        if check.any():
                            rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                            f['errors'].append("The value at row(s) {} must be numeric. \
                            <b>Please avoid adding commas or non-numeric symbols, such as £, £k, £m, %, etc. \
                            If the question does not apply, \
                            please leave it BLANK (avoid adding N/A or comments to this cell)</b>.".format(rows))

                        # Percent
                        df_percent = df_view.loc[df_view['unit']=='percent']
                        try:
                            percent_value = pd.to_numeric(df_percent['value'])
                            # Zero values do not require a warning as they are correct in both scales ([0,1] and [0,100])
                            percent_value = percent_value[percent_value != 0]
                            check = (percent_value >= -1) & (percent_value <= 1)
                            if check.all() and len(check) > 0:
                                rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                                f['warnings'].append("All percent values in this file, row(s) {}, are between -1 and 1. " \
                                                     "This is potentially a mistake. " \
                                                     "Percent values for this part are expected to be in a wider range (e.g., 70, -35). " \
                                                     "Please correct if the values you meant should be in a wider scale.".format(rows))
                        except ValueError:
                            # In case there are non-numeric value and already accounted for above
                            pass

                # Comments
                if 'comment' not in df_input.columns:
                    f['errors'].append('Comment column is missing.')


            ###
            # Save CSV file
            ###
            if not len(f['errors']):
                filename = f['name'].replace('.xlsx', '.csv').replace('.xls', '.csv').replace(' ', '')
                df_input.to_csv(sub_dir_path / filename, encoding='utf-8', index=False, date_format='%d/%m/%Y')

    ###
    # HTML results
    ###
    results = {
        'valid_csv_path': sub_dir_path,
        'weekly_consumer_qualitative_filename': files[0]['name'],
        'weekly_consumer_qualitative_errors': files[0]['errors'],
        'weekly_consumer_qualitative_warnings': files[0]['warnings'],

        'weekly_consumer_quantitative_filename': files[1]['name'],
        'weekly_consumer_quantitative_errors': files[1]['errors'],
        'weekly_consumer_quantitative_warnings': files[1]['warnings'],

        'weekly_financial_qualitative_filename': files[2]['name'],
        'weekly_financial_qualitative_errors': files[2]['errors'],
        'weekly_financial_qualitative_warnings': files[2]['warnings'],

        'weekly_financial_quantitative_filename': files[3]['name'],
        'weekly_financial_quantitative_errors': files[3]['errors'],
        'weekly_financial_quantitative_warnings': files[3]['warnings']
    }

    html_file = html_results(html_output_path, str_now, results)
    # Open the browser with the results
    webbrowser.open(html_file, new=2)


def html_results(html_output_path, str_now, results):
    # pathlib not supported by jinja2's Environment for the used version, hence the use of os.path
    #html_templates_path = Path.cwd() / "covid_19_rfi" / "output_templates"
    #env = Environment(loader=FileSystemLoader(html_templates_path))

    html_templates_dir = os.path.dirname(os.path.abspath(__file__))
    html_templates_dir = os.path.join(html_templates_dir, 'output_templates')
    env = Environment(loader=FileSystemLoader(html_templates_dir))

    template = env.get_template('validation_results_template.html')
    filename = html_output_path / 'validation_results_{}.html'.format(str_now)

    with open(filename, 'w') as fh:
        fh.write(template.render(
            h1="Hello Jinja2",
            show_one=True,
            show_two=False,
            validation_timestamp=str_now,
            valid_csv_path=results['valid_csv_path'],
            weekly_consumer_qualitative_filename=results['weekly_consumer_qualitative_filename'],
            weekly_consumer_qualitative_errors=results['weekly_consumer_qualitative_errors'],
            weekly_consumer_qualitative_warnings=results['weekly_consumer_qualitative_warnings'],

            weekly_consumer_quantitative_filename=results['weekly_consumer_quantitative_filename'],
            weekly_consumer_quantitative_errors=results['weekly_consumer_quantitative_errors'],
            weekly_consumer_quantitative_warnings=results['weekly_consumer_quantitative_warnings'],

            weekly_financial_qualitative_filename=results['weekly_financial_qualitative_filename'],
            weekly_financial_qualitative_errors=results['weekly_financial_qualitative_errors'],
            weekly_financial_qualitative_warnings=results['weekly_financial_qualitative_warnings'],

            weekly_financial_quantitative_filename=results['weekly_financial_quantitative_filename'],
            weekly_financial_quantitative_errors=results['weekly_financial_quantitative_errors'],
            weekly_financial_quantitative_warnings=results['weekly_financial_quantitative_warnings']
        ))

    return filename
