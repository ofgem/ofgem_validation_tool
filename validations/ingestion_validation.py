from validations.data_pipeline import DataPipeline
import pandas as pd
import numpy as np


class IngestionValidation(DataPipeline):

    def calculate(self):
        ingestion_errors = []
        schema_check = [
            {"no_of_columns_check": self.no_of_columns_check()},
            {"missing_obligatory_columns": self.obligatory_columns()},
            {"missing_data_columns": self.miss_data_column_names()},
            {"missing_dict_columns": self.miss_dict_column_names()}]

        # Unrecognised columns (minus internally used column names)
        unrecognised_columns = self.miss_data_column_names() - set(['row_index', 'yes_no_exceptions'])
        if len(unrecognised_columns) > 0:
            error_df = pd.DataFrame([', '.join(unrecognised_columns)], columns=['unrecognised_columns'])
            error_df['error_message'] = 'Please review that the required column headers exist and they use the ' \
                                        'expected spelling with all characters in lower case.'
            ingestion_errors.append(error_df.to_json())

        # Missing columns check
        missing_columns = self.miss_dict_column_names()

        if len(missing_columns) > 0:
            error_df = pd.DataFrame([', '.join(missing_columns)], columns=['missing_columns'])
            error_df['error_message'] = 'Please review that the required column headers exist and they use the ' \
                                        'expected spelling with all characters in lower case.'
            ingestion_errors.append(error_df.to_json())

        # Column order check
        wrong_column_order = self.wrong_column_order()

        if len(wrong_column_order) > 0:
            error_df = pd.DataFrame(wrong_column_order,
                                    columns=['excel_column', 'wrong_column_order', 'expected_column_order'])
            ingestion_errors.append(error_df.to_json())

        print('ingestion check complete ')
        return ingestion_errors if ingestion_errors else None

    def sample_size_check(self):
        return len(self.df[self.row_id_column])

    def no_of_columns_check(self):
        columns_in_data = len(self.df.columns)
        return True if columns_in_data == self.no_of_columns else columns_in_data

    def miss_dict_column_names(self):
        return (set(self.schema_columns)) - (set(self.data_column_names))

    def miss_data_column_names(self):
        return (set(self.data_column_names)) - (set(self.schema_columns))

    def obligatory_columns(self):
        obligatory_columns = [i['to'] for i in self.schema_all if i['optional'] == False]
        return pd.DataFrame([{'missing_columns': np.array(set(obligatory_columns) - set(self.data_column_names))}])

    def get_excel_column(self, n):
        d, m = divmod(n, 26)  # 26 ASCII letters
        return self.get_excel_column(d-1) + chr(m+65) if d else chr(m+65) # chr(65) = 'A'

    def wrong_column_order(self):
        excel_schema_columns = [self.get_excel_column(c) for c in range(self.no_of_columns)]

        # Excel column, data column name, schema column name
        return [(ec, dc, sc) for ec, dc, sc in
                zip(excel_schema_columns, self.data_column_names, self.schema_columns) if dc.lower() != sc.lower()]


if __name__ == "__main__":
    pass