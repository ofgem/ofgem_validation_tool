import re


class Ingest:

    def __init__(self, schema, df, supplier_name):
        self.schema = schema
        self.df = df
        self.supplier_name = supplier_name

    def calculate(self):
        self.df = self.yes_no_converter()

        return self.df

    def yes_no_converter(self):
        boolean = [True, False]

        for column in list(self.df):

            if len(column) > 1:

                col_description = [i['description'] for i in self.schema if re.search(column.replace(" ", "_").lower(), i['to'])]

                for descriptor in col_description:
                    if descriptor is None:
                        continue
                    elif col_description[0] == "Y and N will get converted to true and false after loading data file.":
                        self.df[column] = self.df[column].apply(lambda x: True if str(x) in ['Y', 'yes', 'y', 'YES'] else x)
                        self.df[column] = self.df[column].apply(lambda x: False if str(x) in ['N', 'no', 'n', 'NO'] else x)
                        self.df['yes_no_exceptions'] = self.df[column].isin(boolean)

                    else:
                        continue
        return self.df



if __name__ == "__main__":
    pass
    # Data_pipeline = Ingest(data_file, json_file)
    # Data_pipeline = Ingest(DataPipeline)